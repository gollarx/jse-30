package ru.t1.shipilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @Nullable
    Project create(@NotNull String userId, @NotNull String name);

}
