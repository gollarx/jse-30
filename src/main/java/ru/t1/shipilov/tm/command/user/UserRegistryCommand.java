package ru.t1.shipilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.api.service.IAuthService;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.model.User;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-registry";

    @NotNull
    private final String DESCRIPTION = "Registry user.";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final IAuthService authService = getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
