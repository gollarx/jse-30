package ru.t1.shipilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-lock";

    @NotNull
    private final String DESCRIPTION = "User lock.";

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
